import { Injector, NgModule } from '@angular/core'
import { createCustomElement } from '@angular/elements'
import { BrowserModule } from '@angular/platform-browser'

import { AppComponent } from './app.component'
import { SplineViewerComponent } from './spline-viewer/spline-viewer.component'

@NgModule({
  declarations: [AppComponent, SplineViewerComponent],
  imports: [BrowserModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {
  constructor(private injector: Injector) {
    // const splineViewerWebComponent = createCustomElement(
    //   SplineViewerComponent,
    //   {
    //     injector,
    //   },
    // )
    // customElements.define('spline-viewer', splineViewerWebComponent)
  }
}
