import { Component, Input } from '@angular/core'

@Component({
  selector: 'spline-viewer',
  templateUrl: './spline-viewer.component.html',
  styleUrls: ['./spline-viewer.component.scss'],
})
export class SplineViewerComponent {
  @Input() url!: string
}
